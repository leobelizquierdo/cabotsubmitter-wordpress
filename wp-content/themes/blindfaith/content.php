<?php if ( ! have_posts() ) : ?>
        <h1>Not Found</h1>
            <p>Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post</p>
<?php endif; ?>

<?php while ( have_posts() ) : the_post(); ?>

    <?php show_post(); ?>

<?php endwhile; ?>

<?php if ( $wp_query->max_num_pages > 1 ) : ?>
        <div id="older-posts"><?php next_posts_link('Older Posts'); ?></div>
        <div id="newer-posts"><?php previous_posts_link('Newer Posts'); ?></div>
<?php else: ?>
        <div id="only-page">No newer/older posts</div>
<?php endif; ?>
