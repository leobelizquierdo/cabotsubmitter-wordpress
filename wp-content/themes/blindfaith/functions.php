<?php
/**
 * Setup My Child Theme's textdomain.
 *
 * Declare textdomain for this child theme.
 * Translations can be filed in the /languages/ directory.
 */
function get_content() {
    require_once( get_template_directory() . '/content.php' );
}

function show_post(){
    require_once( get_template_directory() . '/post.php' );
}

add_action( 'get_content', 'get_content' );
add_action( 'show_post', 'show_post' );

?>
