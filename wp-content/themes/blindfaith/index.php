<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang='en'>
    <head>
    <title><?php bloginfo('name'); ?> <?php wp_title(); ?></title>
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/reset.css" type="text/css">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css">
    <!--[if IE]>
    <link rel="stylesheet" href="ie.css" type="text/css" />
    <![endif]-->
    </head>
    <body class="home blog">
        <?php get_header(); ?>
    <div id="wrapper">
        <div id="content">
            <?php get_content(); ?>
        <div class="spacer"></div>
            <?php get_footer(); ?>
        </div><!-- content -->
            <?php get_sidebar(); ?>
        <div class="spacer"></div>
    </div><!-- wrapper -->
    </body>
</html>
