<div id="top-bar-tile">
    <div id="top-bar-content">
        <h1><a href="<!--?php bloginfo('url'); ?-->"><?php bloginfo('name'); ?></a></h1>
        <span class="slogan"><?php bloginfo('description'); ?></span>
        <div id="search-box">
            <form method="get" id="searchform" action="" >
                <input type="text" value="Search..." onfocus="if(this.value == this.defaultValue) this.value = ''" name="s" id="s" />
            </form>
        </div><!-- search-box -->
    </div><!-- top-bar-content -->
</div><!-- top-bar-tile -->
<div id="nav-bar-tile">
         <div class="nav-bar-content"><ul><li class="current_page_item"><a href="http://www.danwalker.com/themes/blindfaith/" title="Home">Home</a></li><li class="page_item page-item-2"><a href="http://www.danwalker.com/themes/blindfaith/?page_id=2" title="About">About</a></li><li class="page_item page-item-4"><a href="http://www.danwalker.com/themes/blindfaith/?page_id=4" title="Typography">Typography</a></li></ul></div>
</div><!-- nav-bar-tile -->
